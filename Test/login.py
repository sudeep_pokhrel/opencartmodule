import unittest
from selenium import webdriver
from PageObjects.BasePage import BasePage
from PageObjects.CaseRun import CaseRun


class MyTestCase(unittest.TestCase):
    def setUp(self):
      self.driver=webdriver.Chrome('C:\selenium\chromedriver.exe')
      driver=self.driver
      url= BasePage(driver)
      url.callWebsite()


    def test_login(self):
        driver= self.driver
        login = CaseRun(driver)
        username = "jordan123"
        uservalue = login.login(username,"password")
        self.assertEqual(uservalue,"Welcome "+username)
        # self.driver.savescreenshot("C:\Users\lenovo\PycharmProjects\automationtest\Test\screenshot")
    def tearDown(self):
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
