import unittest
from selenium import webdriver
from PageObjects.GooglePage import HomePage

class MyTestCase(unittest.TestCase):
 def setUp(self):
    self.driver=webdriver.Chrome('C:\selenium\chromedriver.exe')
    self.base_url = "https://www.google.com"

    def test_google_search(self):
        driver = self.driver
        driver.get(self.base_url)
        googlepage = HomePage(driver)
        googlepage.enter_search_text("Broadways")
        googlepage.click_key_search()


     def tearDown(self):
         self.driver.close()




if __name__ == '__main__':
    unittest.main()
