import unittest

from selenium import webdriver
import pytest



class MyTestCase(unittest.TestCase):
     @pytest.fixture()
     def setUp(self):
            self.driver=webdriver.Chrome('C:\selenium\chromedriver.exe')
            self. driver.maximize_window()
            print("setup executed")
            yield
            self.driver.close()


     def test_login(self):

             self.driver.get("https://demo.opencart.com/admin")
             driver=self.driver
             driver.implicitly_wait(10)
             username = driver.find_element_by_id("input-username")
             username.clear()
             username .send_keys("demo")
             password = driver.find_element_by_id("input-password")
             password.clear()
             password.send_keys("demo")
             login = driver.find_element_by_xpath("//*[@id='content']/div/div/div/div/div[2]/form/div[3]/button")
             login.click()



     @pytest.fixture()
     def  tearDown(self):
        self.driver.close()



if __name__ == '__main__':
    unittest.main()
