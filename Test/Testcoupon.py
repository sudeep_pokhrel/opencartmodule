import unittest
from selenium import webdriver
from PageObjects.BasePage import BasePage
from PageObjects.coupon import couponMOdule
import  time
import HtmlTestRunner
import pytestpytest


class MyTestCase(unittest.TestCase):

    def setUp(self):
      self.driver=webdriver.Chrome('C:\selenium\chromedriver.exe')
      driver=self.driver
      url= BasePage(driver)
      url.Website()

    def test_login(self) :
        driver =self.driver
        login =couponMOdule(driver)
        userName ="demo"
        passWord ="demo"
        login.login(userName,passWord)
        self.assertTrue(True)
        # opencart =driver.find_element_by_xpath("//*[@id='header-logo']/a/img")
        # self.assertFalse(opencart=="//*[@id='header-logo']/a/img")

    def test_coupon(self):
        driver =self.driver
        coupon=couponMOdule(driver)
        username = "demo"
        coupon.login(username,"demo")
        couponname ="SYRDG6"
        code ="789d"
        type="23"
        discount ="12"
        total_amount ="12"
        products ="Apple"
        category="Camera"
        date_start="2021-07-3"
        date_end ="2021-04-9"
        uses_per_coupon="2"
        uses_per_customer="5"
        coupon.contact(couponname,code,type,discount,total_amount,products,category,date_start,date_end,uses_per_coupon,uses_per_customer)
        self.assertTrue(True)
    def test_email(self):
        driver = self.driver
        email=couponMOdule(driver)
        username = "demo"
        email.login(username,"demo")
        message_to = "All Customers"
        subject = "Hi How r you ?I hope u r doing well"
        message = "Test"
        email.mail(message_to,subject,message)
        self.assertTrue(True)



    def test_marketing(self):
        driver =self.driver
        marketing = couponMOdule(driver)
        username = "demo"
        marketing.login(username,"demo")
        campaign_name ="Sudeep"
        description = "Test "
        tracking = "345Es"
        examples ="phhnj7t"
        marketing.Marketing(campaign_name,description,tracking,examples)
        self.assertTrue(True)
    # def tearDownClass(self):
    #     self.driver.close()






if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='C:Users\lenovo\PycharmProjects\automationtest\Report'))
