from selenium import webdriver
from selenium .common.exceptions import ElementNotInteractableException
from selenium .webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

import  time

driver=webdriver.Chrome('C:\selenium\chromedriver.exe')
driver.maximize_window()

driver.get("https://demo.opencart.com/admin")

time.sleep(5)
username = driver.find_element_by_id("input-username")
username.clear()
username .send_keys("demo")


password = driver.find_element_by_id("input-password")
password.clear()
password.send_keys("demo")

login = driver.find_element_by_xpath("//*[@id='content']/div/div/div/div/div[2]/form/div[3]/button")
login.click()
time.sleep(5)

marketing = driver.find_element_by_xpath("//*[@id='menu-marketing']/a")
marketing.click()

driver.implicitly_wait(10)
coupons = driver.find_element_by_xpath("/html/body/div[1]/nav/ul/li[7]/ul/li[2]/a")
coupons.click()

couponsadd = driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/a")
couponsadd.click()

time.sleep(5)
couponname =driver.find_element_by_id("input-name")
couponname.send_keys("Test")

code =driver.find_element_by_id("input-code")
code.send_keys("SER23")

discount=driver.find_element_by_id("input-discount")
discount.send_keys("29")

total_amount =driver.find_element_by_id("input-total")
total_amount.send_keys("8902")

customer_login =driver.find_element_by_xpath("//*[@id='tab-general']/div[6]/div/label[1]/input")
customer_login.click()

free_shipping =driver.find_element_by_xpath("//*[@id='tab-general']/div[7]/div/label[1]/input")
free_shipping.click()

time.sleep(5)
products =driver.find_element_by_name("product").send_keys("Apple")
products = driver.find_element_by_name("product").send_keys(Keys.ENTER)

coupon =driver.find_element_by_name("category").send_keys("Camera")
coupon = driver.find_element_by_name("category").send_keys(Keys.ENTER)

date_start=driver.find_element_by_xpath("//*[@id='input-date-start']")
date_start .clear()
date_start.send_keys("2021-07-06 ")

date_end=driver.find_element_by_name("date_end")
date_end.clear()
date_end.send_keys("2021-09-04")

uses_per_coupon =driver.find_element_by_name("uses_total")
uses_per_coupon.clear()
uses_per_coupon.send_keys("2")

uses_per_customer=driver.find_element_by_name("uses_customer")
uses_per_customer.clear()
uses_per_customer.send_keys("1")

status=driver.find_element_by_id("input-status")
status.click()

driver.delete_all_cookies()
save  =driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/button")
save.click()

