from selenium import webdriver
import pytest

class TestOrangeHRM() :

    @pytest.fixture()
    def setup(self):
        global driver
        self.driver = webdriver.Chrome("C:\seleniumn\chromedriver.exe")
        self.driver.maximize_window()
        yield
        self.driver.close()

    def test_home_title(self,setup):
        driver = self.driver
        driver.get("https://opensource-demo.orangehrmlive.com")
        assert driver.title == "OrangeHRM"

    def login(self,setup):
        driver=self.driver
        driver.get("https://opensource-demo.orangehrmlive.com")
        driver.find_element_by_id("txtUsername").send_keys("Admin")
        driver.find_element_by_id("txtPassword").send_keys("admin123")
        driver.find_element_by_id("btnLogin").click()
        assert driver.title == "OrangeHRM"
