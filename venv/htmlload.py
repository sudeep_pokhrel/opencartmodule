import unittest
import HtmlTestRunner
from selenium import webdriver


class MyTestCase(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls.driver=webdriver.Chrome('C:\selenium\chromedriver.exe')
        cls.driver.implicitly_wait(10)
        cls. driver.maximize_window()
        print("setup executed")

    def test_example(self):
        self.driver .get("https://www.google.com")
        titleofweb =self.driver.title
        self.assertTrue("Google" == titleofweb)
        self.driver.find_element_by_name('q').send_keys('Nepal')
        self.driver.find_element_by_name('btnK').click()
        print("Nepal  search ")

    def test_example1(self):
        self.driver.get("https://www.google.com")
        titleofweb =self.driver.title
        self.assertTrue("Google"==titleofweb)
        self.driver.find_element_by_name('q').send_keys('Nararik')
        self.driver.find_element_by_name('btnK').click()
        print("Nagarik news")
        self.assertTrue(True)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        print("test close")

if __name__ == '__main__':
        unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='C:Users\lenovo\PycharmProjects\automationtest\Reports'))
