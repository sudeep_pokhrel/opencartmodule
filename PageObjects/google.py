from selenium.webdriver.common.keys import Keys
class HomePage:

    def __init__(self,driver):
        self.driver = driver

    def search_text(self, searchtext):
        return self.driver.find_element_by_name("q").send_keys(searchtext)

    def click_text(self):
        return self.driver.find_element_by_name("q").send_keys(Keys.ENTER)
