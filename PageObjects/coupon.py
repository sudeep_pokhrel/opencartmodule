import time
from selenium .webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common .by import By
from selenium.webdriver.support import expected_conditions as EC


class couponMOdule:
    def __init__(self,driver):
        self.driver = driver


    def login(self,userName,passWord):
         driver =self.driver
         driver.implicitly_wait(10)
         username = driver.find_element_by_id("input-username")
         username.clear()
         username .send_keys(userName)


         password = driver.find_element_by_id("input-password")
         password.clear()
         password.send_keys(passWord)

         login = driver.find_element_by_xpath("//*[@id='content']/div/div/div/div/div[2]/form/div[3]/button")
         login.click()

         # opencart =driver.find_element_by_xpath("//*[@id='header-logo']/a/img")
         # return opencart


    def Marketing(self,campaign_name,description,tracking,examples):
        driver=self.driver
        driver.implicitly_wait(10)
        marketing = driver.find_element_by_xpath("//*[@id='menu-marketing']/a")
        marketing.click()

        marketingmodule = driver.find_element_by_xpath("/html/body/div[1]/nav/ul/li[7]/ul/li[1]/a")
        marketingmodule.click()

        marketingmoduleadd = driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/a")
        marketingmoduleadd.click()

        campaign =driver.find_element_by_name("name")
        campaign.send_keys(campaign_name)

        campaigndesc =driver.find_element_by_id("input-description")
        campaigndesc.send_keys(description)
        trackingcode=driver.find_element_by_id("input-code")
        trackingcode.send_keys(tracking)
        example =driver.find_element_by_id("input-example1")
        example.send_keys(examples)
        driver.implicitly_wait(10)
        save=driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/button")
        save.click()
        return True




    def contact(self,couponName,coDe,type,discount,total_amount,products,category,date_start,date_end,uses_per_coupon,uses_per_customer):
        driver=self.driver
        marketing = driver.find_element_by_xpath("//*[@id='menu-marketing']/a")
        marketing.click()

        driver.implicitly_wait(10)
        coupons = driver.find_element_by_xpath("/html/body/div[1]/nav/ul/li[7]/ul/li[2]/a")
        coupons.click()

        couponsadd = driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/a")
        couponsadd.click()


        couponname =driver.find_element_by_id("input-name")
        couponname.send_keys(couponName)

        code =driver.find_element_by_id("input-code")
        code.send_keys(coDe)

        types =driver.find_element_by_xpath("//*[@id='input-type']")
        types.send_keys(type)

        discounts=driver.find_element_by_id("input-discount")
        discounts.send_keys(discount)

        total_amounts =driver.find_element_by_id("input-total")
        total_amounts.send_keys(total_amount)

        customer_logins =driver.find_element_by_xpath("//*[@id='tab-general']/div[6]/div/label[1]/input")
        customer_logins.click()

        free_shippings =driver.find_element_by_xpath("//*[@id='tab-general']/div[7]/div/label[1]/input")
        free_shippings.click()

        product =driver.find_element_by_name("product").send_keys(products)
        product = driver.find_element_by_name("product").send_keys(Keys.ENTER)

        categorys =driver.find_element_by_name("category").send_keys(category)
        categorys = driver.find_element_by_name("category").send_keys(Keys.ENTER)

        date_starts=driver.find_element_by_xpath("//*[@id='input-date-start']")
        date_starts .clear()
        date_starts.send_keys(date_start)

        date_ends=driver.find_element_by_name("date_end")
        date_ends.clear()
        date_ends.send_keys(date_end)

        uses_per_coupons =driver.find_element_by_name("uses_total")
        uses_per_coupons.clear()
        uses_per_coupons.send_keys(uses_per_coupon)

        uses_per_customers=driver.find_element_by_name("uses_customer")
        uses_per_customers.clear()
        uses_per_customers.send_keys(uses_per_customer)

        status= driver.find_element_by_id("input-status")
        status.click()

        save  =driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/button")
        save.click()

    def mail(self,message_to,message,subject):
        driver=self.driver

        # mail = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.xpath,"recipient-email")))
        # self.driver.implicitly_wait(10)

        marketing = driver.find_element_by_xpath("//*[@id='menu-marketing']/a")
        marketing.click()

        mail = driver.find_element_by_xpath("//*[@id='collapse38']/li[3]/a")
        mail.click()

        message_from =driver.find_element_by_id("input-store").send_keys(Keys.ENTER)

        messages_to =driver.find_element_by_name("subject").send_keys(message_to)
        messages_to = driver.find_element_by_name("subject").send_keys(Keys.ENTER)


        subjects = driver.find_element_by_xpath("//*[@id='input-subject']")
        subjects.send_keys(subject)

        messages=driver.find_element_by_xpath("//*[@id='content']/div[2]/div/div[2]/form/div[8]/div/div/div[3]/div[2]")
        messages.send_keys(message)

        save  =driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/button")
        save.click()






