from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium .webdriver.common.by import By

import time

class CaseRun:
    def __init__(self,driver):
        self.driver = driver

    def login(self,username,password):
        login = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.ID,"login2")))
        self.driver.implicitly_wait(10)
        login.click()
        Username =self.driver.find_element_by_xpath("//*[@id='loginusername']")
        Username.send_keys(username)
        Password =self.driver.find_element_by_xpath("//*[@id='loginpassword']")
        Password.send_keys(password)
        login =self.driver.find_element_by_xpath("//*[@id='logInModal']/div/div/div[3]/button[2]")
        login.click()
        time.sleep(5)
        uservalue =self.driver.find_element_by_id("nameofuser").text
        return uservalue


    def Contact(self,email,name,message):
     contact =self.driver.find_element_by_link_text("Contact")
     self.driver.implicity_wait(10)
     contact.click()
     driver =self.driver
     ContactEmail = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.ID,"recipient-email")))
     self.driver.implicitly_wait(10)
     ContactEmail.send_keys(email)
     ContactName = driver.find_element_by_id("recipient-name")
     ContactName.send_keys(name)
     Messages = driver.find_element_by_id("message_text")
     Messages.send_keys(message)
     SendMessage = driver.find_element_by_xpath("//*[@id='exampleModal']/div/div/div[3]/button[2]")
     SendMessage.click()
     obj = driver.switch_to.alert
     obj.accept()
     print(self.text)
     return True
